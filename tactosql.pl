#tactosql.pl
#created by _KUL (Kuleshov)
#version 2016-10-26
#Apache License 2.0

#!/bin/perl
use warnings;
use strict;
use File::stat;
use v5.14;
use Data::Dumper;
use POSIX qw(strftime);
use DBI;
use utf8;

#
# Created by Kuleshov. Version 1_26.10.2016
#

my $acct_dir = '/var/log/tac_plus/acct/';
my $access_dir = '/var/log/tac_plus/access/';
my @ignoreservice = (
				'ppp',
			);
my @ignoreinitiator = (
				'EEM:syslog-int-test',
			);

my $dbh = DBI->connect("DBI:mysql:database=tacacs;host=127.0.0.1","tacacs","tacacs9") or die $DBI::errstr;

#начало бесконечног цикла
while (1) {
$dbh = DBI->connect_cached("DBI:mysql:database=tacacs;host=127.0.0.1","tacacs","tacacs9") or die $DBI::errstr;

if (defined($acct_dir)) {
	my $mode = 1;
	my %files;
	opendir (DIR, $acct_dir) or die $!;
	while (my $f = readdir(DIR)) {
		next if $f =~ /^\./;
		my $st = stat("$acct_dir"."$f") or die $!;
		$files{$f}->{size} = $st->size();
		$files{$f}->{mtime} = strftime("%Y-%m-%d %H:%M:%S", localtime($st->mtime()));
		$files{$f}->{lastline} = 0;
		$files{$f}->{lastread} = '1000-01-01 00:00:00';
		$files{$f}->{lastsize} = 0;
	}
	closedir DIR;

	my $sth = $dbh->prepare(qq(SELECT * FROM files WHERE mode = '1'));
	$sth->execute() or die $dbh->errstr();
	while (my $ref = $sth->fetchrow_hashref()) {
		if (exists $files{$ref->{name}}) {
			$files{$ref->{name}}->{lastline} = $ref->{lastline};
			$files{$ref->{name}}->{lastread} = $ref->{lastread};
			$files{$ref->{name}}->{lastsize} = $ref->{lastsize};
		}
	}
	foreach my $h (sort keys %files) {
		if ($files{$h}->{lastread} lt $files{$h}->{mtime}) {
			open (FILE, "<" ,$acct_dir.$h) or die $!;
			my ($t,$line,$trash, @tmp,@time,@split, %entry);
			if (exists($files{$h}->{lastline}) and ($files{$h}->{size} > $files{$h}->{lastsize})) {
				$line = $files{$h}->{lastline};
			} else {
				$line = 0;
			}
			my $counter = 0;
			while (<FILE>) {
				$counter++;
				next if $counter <= $line;
				$trash = "";
				@tmp = split(/\t/,$_);
				@time = split(/ /,$tmp[0]);
				$entry{time} = \@time;
				$entry{device_ip} = $tmp[1];
				$entry{device_ip} =~ s/(\s|\t|\n)+$//;
				$entry{user} = $tmp[2];
				$entry{user} =~ s/(\s|\t|\n)+$//;
				$entry{initiator} = $tmp[4];
				$entry{initiator} =~ s/(\s|\t|\n)+$//;
				undef %{$entry{trash}};
				foreach my $elem(6..$#tmp) {
					@split = split(/=/,$tmp[$elem],2);
					$entry{trash}->{$split[0]} = $split[1];
					$trash .= $split[0]."=".$split[1]."</br>\n" if (($split[0] ne "service") and ($split[0] ne "cmd"));
				}
				if (defined($entry{trash}->{service}) and (!grep(/$entry{trash}->{service}/,@ignoreservice)) and (!grep(/$entry{initiator}/,@ignoreinitiator))) {
					$entry{trash}->{service} =~ s/(\s|\t|\n)+$//;
					$t = $entry{time}->[0]." ".$entry{time}->[1];
					$sth = $dbh->prepare(qq'INSERT INTO acct (time,device_ip,user,initiator,service,cmd,trash) VALUES (?,?,?,?,?,?,?)');
					$sth->execute($t, $entry{device_ip}, $entry{user}, $entry{initiator}, $entry{trash}->{service}, $entry{trash}->{cmd}, $trash) or die $dbh->errstr();
				}
			}
			$t = strftime("%Y-%m-%d %H:%M:%S", localtime());
			$sth = $dbh->prepare(qq'REPLACE INTO files (name, lastread, lastline, lastsize, mode) VALUES (?,?,?,?,?)');
			$sth->execute($h, $t, $counter, $files{$h}->{size},"1") or die $dbh->errstr();
			close FILE;
		}
	}
}

if (defined($access_dir)) {
	my $mode = 2;
	my %files;
	opendir (DIR, $access_dir) or die $!;
	while (my $f = readdir(DIR)) {
		next if $f =~ /^\./;
		my $st = stat("$access_dir"."$f") or die $!;
		$files{$f}->{size} = $st->size();
		$files{$f}->{mtime} = strftime("%Y-%m-%d %H:%M:%S", localtime($st->mtime()));
		$files{$f}->{lastline} = 0;
		$files{$f}->{lastread} = '1000-01-01 00:00:00';
		$files{$f}->{lastsize} = 0;
	}
	closedir DIR;

	my $sth = $dbh->prepare(qq(SELECT * FROM files WHERE mode = '2'));
	$sth->execute() or die $dbh->errstr();
	while (my $ref = $sth->fetchrow_hashref()) {
		if (exists $files{$ref->{name}}) {
			$files{$ref->{name}}->{lastline} = $ref->{lastline};
			$files{$ref->{name}}->{lastread} = $ref->{lastread};
			$files{$ref->{name}}->{lastsize} = $ref->{lastsize};
		}
	}
	foreach my $h (sort keys %files) {
		if ($files{$h}->{lastread} lt $files{$h}->{mtime}) {
			open (FILE, "<", $access_dir.$h) or die $!;
			my ($t,$line, @time,@tmp, %entry);
			if (exists($files{$h}->{lastline}) and ($files{$h}->{size} > $files{$h}->{lastsize})) {
				$line = $files{$h}->{lastline};
			} else {
				$line = 0;
			}
			my $counter = 0;
			while (<FILE>) {
				$counter++;
				next if $counter <= $line;
				@tmp = split(/\t/,$_);
				@time = split(/ /,$tmp[0]);
				$entry{time} = \@time;
				@tmp = split(" ",$tmp[1]);
				$entry{device_ip} = $tmp[0];
				$entry{device_ip} =~ s/://m;
				$entry{device_ip} =~ s/(\s|\t|\n)+$//;
				$entry{user} = $tmp[4];
				$entry{user} =~ s/'//g;
				$entry{user} =~ s/(\s|\t|\n)+$//;
				$entry{initiator} = $tmp[6];
				$entry{initiator} =~ s/(\s|\t|\n)+$//;
				$entry{result} = "";
				foreach my $elem(9..$#tmp) {
					$entry{result} .= $tmp[$elem]." ";
				}
				$entry{result} =~ s/(\s|\t|\n)+$//;
				if (defined($entry{initiator}) and (!grep(/$entry{initiator}/,@ignoreinitiator))) {
					$t = $entry{time}->[0]." ".$entry{time}->[1];
					$sth = $dbh->prepare(qq'INSERT INTO access (time,device_ip,user,initiator,result) VALUES (?,?,?,?,?)');
					$sth->execute($t, $entry{device_ip}, $entry{user}, $entry{initiator}, $entry{result}) or die $dbh->errstr();
				}
			}
			$t = strftime("%Y-%m-%d %H:%M:%S", localtime());
			$sth = $dbh->prepare(qq'REPLACE INTO files (name, lastread, lastline, lastsize, mode) VALUES (?,?,?,?,?)');
			$sth->execute($h, $t, $counter, $files{$h}->{size},"2") or die $dbh->errstr();
			close FILE;
		}
	}
}

#конец бесконечног цикла
sleep 60;
}
