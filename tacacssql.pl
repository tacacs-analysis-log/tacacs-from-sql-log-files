#tacacssql.pl
#created by _KUL (Kuleshov)
#version 2016-10-26
#Apache License 2.0

#!C:\Strawberry\perl\bin\perl.exe
use Plack::Loader;
my $app = Plack::Util::load_psgi("tacacssql.psgi");
Plack::Loader->auto->run($app);