﻿-- --------------------------------------------------------
-- Версия сервера:               5.5.50-MariaDB - MariaDB Server
-- ОС Сервера:                   Linux
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных tacacs
CREATE DATABASE IF NOT EXISTS `tacacs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tacacs`;


-- Дамп структуры для таблица tacacs.access
CREATE TABLE IF NOT EXISTS `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `device_ip` varchar(15) DEFAULT NULL,
  `user` varchar(30) DEFAULT NULL,
  `initiator` varchar(30) DEFAULT NULL,
  `result` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `device_ip` (`device_ip`),
  KEY `time` (`time`),
  KEY `initiator` (`initiator`),
  KEY `result` (`result`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица tacacs.acct
CREATE TABLE IF NOT EXISTS `acct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `device_ip` varchar(15) DEFAULT NULL,
  `user` varchar(30) DEFAULT NULL,
  `initiator` varchar(30) DEFAULT NULL,
  `service` varchar(30) DEFAULT NULL,
  `cmd` text,
  `trash` text,
  PRIMARY KEY (`id`),
  KEY `time` (`time`),
  KEY `device_ip` (`device_ip`),
  KEY `user` (`user`),
  KEY `initiator` (`initiator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица tacacs.files
CREATE TABLE IF NOT EXISTS `files` (
  `name` varchar(100) NOT NULL,
  `lastread` datetime DEFAULT NULL,
  `lastline` int(11) DEFAULT NULL,
  `lastsize` int(11) DEFAULT '0',
  `mode` tinyint(4) NOT NULL,
  PRIMARY KEY (`name`,`mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
