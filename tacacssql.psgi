#tacacssql.psgi
#created by _KUL (Kuleshov)
#version 2016-10-26
#Apache License 2.0

#!/bin/perl
use strict;
use warnings;
use utf8;
use v5.14;
use Plack;
use Plack::Request;
use Plack::Builder;
use POSIX qw(strftime);
use DBI;

my ($req,$rawhtml,$fileisok,$refgmass,$mode,$html);
my $dbh = DBI->connect("DBI:mysql:database=tacacs;host=172.17.1.1","tacacs","tacacs") or die $DBI::errstr;

#главное приложение
my $app = sub {
	my $env = shift;
	$req = Plack::Request->new($env);
	$html = "";
	$html .= q~<div style="font-size:12px; width:100%; text-align:right">Created by Kuleshov. Version 1_26.10.2016</div>~;
	if (!defined($req->param('mode'))) {
		$html .= show_mode();
	} else {
		$mode = $req->param('mode');
		$html .= qq~<a href="${$req->base}"><< назад</a><br><br>~;
		my $var;
		if (!defined($req->param('from')) or !defined($req->param('from'))) {
			$var->[1] = strftime("%Y-%m-%d %H:%M:%S", localtime()) if (!defined($var->[1]));
			$var->[0] = strftime("%Y-%m-%d %H:%M:%S", localtime(time()-86400)) if (!defined($var->[0]));
		} else {
			$var->[1] = $req->param('to');
			$var->[0] = $req->param('from');
		}
		$html .= get_form_date([$var->[0],$var->[1]]);
		$html .= get_form_params([$mode,$var->[0],$var->[1]]);
	}
	if (defined($req->param('action')) and ($req->param('action') eq "show")) {
		$html .= get_content();
	};
	return [
		'200',
		['Content-Type' => 'text/html', 'Charset' => 'UTF-8'],
		[ get_html("top"), $html, get_html("bottom")],
	];
};

#функция выбора режима
sub show_mode {
	$rawhtml = q~
				<table align=center>
					<tr>
						<td>
							<a href="?mode=log">Статистика событий</a>
						</td>
						<td width=10px>
						</td>
						<td color=>
							<a href="?mode=auth">Статистика авторизаций</a><br>
						</td>
					</tr>
				</table>
			~;
	return $rawhtml;
}

#функции главного приложения
sub get_form_date {
	my $var = shift;
	$var->[1] = strftime("%Y-%m-%d %H:%M:%S", localtime()) if (!defined($var->[1]));
	$var->[0] = strftime("%Y-%m-%d %H:%M:%S", localtime(time()-86400)) if (!defined($var->[0]));
	return qq~
				<div>
					<form action=tacacssql.pl method=GET>
						<input type="datetime" name="from" placeholder="yyyy-mm-dd hh:mm:ss" value="$var->[0]" pattern="([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9]) ([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])" required>
						<input type="datetime" name="to" placeholder="yyyy-mm-dd hh:mm:ss" value="$var->[1]" pattern="([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9]) ([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])" required>
						<input hidden type="input" name="mode" value="$mode">
						<input type="submit" value="Выбрать">
					</form>
				</div>
				~;
}

sub get_form_params {
	my $f = shift;
	my ($tmp,$tmpstr,@result,@inserted,@device_ip,@user,@initiator,@service);
	#обрабатываем системные логи
	if ($f->[0] eq "log") {
		my $sth = $dbh->prepare(qq(SELECT device_ip,user,initiator,service FROM acct WHERE time BETWEEN "$f->[1]" and "$f->[2]" ORDER by time));
		$sth->execute() or die $dbh->errstr();
		while (my $ref = $sth->fetchrow_hashref()) {
			push(@user,$ref->{user});
			push(@device_ip,$ref->{device_ip});
			push(@initiator,$ref->{initiator});
			push(@service,$ref->{service});
		}
		$rawhtml .= qq~
				<div style="width:500px; margin-top:15px; padding:5px; border-radius:10px; background: linear-gradient(to top left, #90FFD3, #F2FFFA); box-shadow: 0 0 5px #ABABAB">
					<form action=tacacssql.pl method=GET>
					<input hidden name="mode" value="$f->[0]">
					<input hidden name="action" value="show">
					<input hidden name="from" value="$f->[1]">
					<input hidden name="to" value="$f->[2]">
					<table>
				~;
		$rawhtml .= q~	<tr>
							<td>
								Устройство:
							</td>
							<td>
								<select name="device">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('device'))) {
			$tmp = $req->param('device');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @device_ip) {
			if (!defined($inserted[0]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[0]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						~;
		$rawhtml .= q~	<tr>
							<td>
								Пользователь:
							</td>
							<td> 
								<select name="user">
								<option value="">без фильтра</option>
						~;

		if (defined($req->param('user'))) {
			$tmp = $req->param('user');
		} else {
			$tmp = "";
		};

		foreach my $ref (sort @user) {
			if (!defined($inserted[1]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[1]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						~;
		$rawhtml .= q~	<tr>
							<td>
								Источник:
							</td>
							<td> 
								<select name="initiator">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('initiator'))) {
			$tmp = $req->param('initiator');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @initiator) {
			if (!defined($inserted[2]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[2]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						~;
		$rawhtml .= q~	<tr>
							<td>
								Сервис:
							</td>
							<td> 
								<select name="service">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('service'))) {
			$tmp = $req->param('service');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @service) {
			if (!defined($inserted[3]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[3]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Расширенные свойства:
							</td>
							<td>
					~;
		$tmpstr = "";
		if (defined($req->param('extended')) and ($req->param('extended') eq "on")) {
			$tmpstr = " checked";
		}
		$rawhtml .= qq~			<input type="checkbox" name="extended"$tmpstr>
							</td>
						</tr>
						</table>
						<i>Крайне не желательно оставлять абсолютно все фильтры пустыми!<br>
						Это вызывает большую нагрузку на сервер и Ваш браузер.</i><br>
						<input type="submit" value = "Сформировать">
					</form>
				</div>
				~;
		
		return $rawhtml;
			
		#обрабатываем логи авторизации	
	} 
	if ($f->[0] eq "auth") {
		my $sth = $dbh->prepare(qq(SELECT device_ip,user,initiator,result FROM access WHERE time BETWEEN "$f->[1]" and "$f->[2]" ORDER by time));
		$sth->execute() or die $dbh->errstr();
		while (my $ref = $sth->fetchrow_hashref()) {
			push(@user,$ref->{user});
			push(@device_ip,$ref->{device_ip});
			push(@initiator,$ref->{initiator});
			push(@result,$ref->{result});
		}
		$rawhtml = qq~
				<div style="width:500px; margin-top:15px; padding:5px; border-radius:10px; background: linear-gradient(to top left, #90DFFF, #F2FBFF); box-shadow: 0 0 5px #ABABAB">
					<form action=tacacssql.pl method=GET>
					<input hidden name="mode" value="$f->[0]">
					<input hidden name="action" value="show">
					<input hidden name="from" value="$f->[1]">
					<input hidden name="to" value="$f->[2]">
					<table>
				~;
		$rawhtml .= q~	<tr>
							<td>
								Устройство:
							</td>
							<td>
								<select name="device">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('device'))) {
			$tmp = $req->param('device');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @device_ip) {
			if (!defined($inserted[0]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[0]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
					~;
		$rawhtml .= q~	<tr>
							<td>
								Пользователь:
							</td>
							<td> 
								<select name="user">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('user'))) {
			$tmp = $req->param('user');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @user) {
			if (!defined($inserted[1]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[1]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						~;
		$rawhtml .= q~	<tr>
							<td>
								Источник:
							</td>
							<td> 
								<select name="initiator">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('initiator'))) {
			$tmp = $req->param('initiator');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @initiator) {
			if (!defined($inserted[2]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[2]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						~;
		$rawhtml .= q~	<tr>
							<td>
								Результат:
							</td>
							<td> 
								<select name="result">
								<option value="">без фильтра</option>
						~;
		if (defined($req->param('result'))) {
			$tmp = $req->param('result');
		} else {
			$tmp = "";
		};
		foreach my $ref (sort @result) {
			if (!defined($inserted[3]->{$ref})) {
				$tmpstr = "";
				$tmpstr = " selected" if ($ref eq $tmp);
				$rawhtml .= qq~<option value="$ref"$tmpstr>$ref</option>~;
				$inserted[3]->{$ref} = 1;
			}
		}
		$rawhtml .= q~
								</select>
							</td>
						</tr>
						</table>
						<i>Крайне не желательно оставлять абсолютно все фильтры пустыми!<br>
						Это вызывает большую нагрузку на сервер и Ваш браузер.</i><br>
						<input type="submit" value = "Сформировать">
						</form>
					</div>
				~;
	}
}

sub get_content {
	my $f = shift;
	my ($tmpstr, $tmp, $extended);
	if ($req->param('mode') eq "log") {
		$refgmass->{from} = $req->param('from');
		$refgmass->{to} = $req->param('to');
		$rawhtml = qq~	<br>
					<table border=0 >
					<tr style=\"background:#7AE3A6\">
						<td class="cell"><b>Время</b></td>
						<td class="cell"><b>Устройство</b></td>
						<td class="cell"><b>Пользователь</b></td>
						<td class="cell"><b>Источник</b></td>
						<td class="cell"><b>Сервис</b></td>
						<td class="cell"><b>Действие</b></td>
						<td class="cell"><b>Расширенные свойства</b></td>
					</tr>
						~;
		my $colorflag = 0;
		my $colorrow = "";
		$tmp = "";
		my $tmpuser;
		if ($req->param('device')) { $tmp .= qq~ AND device_ip = "${\$req->param('device')}"~;}
		if ($req->param('user')) { $tmpuser = qq~ AND user = "${\$req->param('user')}"~; $tmpuser =~ s/\\/\\\\/g; $tmp .= $tmpuser;}
		if ($req->param('initiator')) { $tmp .= qq~ AND initiator = "${\$req->param('initiator')}"~;}
		if ($req->param('service')) { $tmp .= qq~ AND service = "${\$req->param('service')}"~;}
		if (defined($req->param('extended')) and ($req->param('extended') eq "on")) {$extended = ",trash"} else {$extended = "";}
		my $sth = $dbh->prepare(qq(SELECT time,device_ip,user,initiator,service,cmd$extended FROM acct WHERE (time BETWEEN "$refgmass->{from}" and "$refgmass->{to}") $tmp ORDER by time));
		$sth->execute() or die $rawhtml .= $dbh->errstr();
		while (my $ref = $sth->fetchrow_hashref()) {
			if ($colorflag eq 0) {
				$colorrow = "#DAFFEA";
				$colorflag = 1;
			} else {
				$colorrow = "#AEF2CA"; 
				$colorflag = 0;
			} ;
			$rawhtml .= qq~
							<tr style=\"background:$colorrow\">
								<td class="cell">
									$ref->{time}
								</td>
								<td class="cell">
									$ref->{device_ip}
								</td>
								<td class="cell">
									$ref->{user}
								</td>
								<td class="cell">
									$ref->{initiator}
								</td>
								<td class="cell">
									$ref->{service}
								</td>
								<td class="cell">
									$ref->{cmd}
								</td>
						~;
			if (defined($req->param('extended')) and ($req->param('extended') eq "on")) {
			$rawhtml .= qq~
							<td class="cell">
								$ref->{trash}
							</td>
						</tr>
					~;
			} else {
				$rawhtml .= qq~
							<td>
							</td>
						</tr>
					~;
			}
		}
		$rawhtml .= qq~
						</table>
						~;
	}
	if ($req->param('mode') eq "auth") {
		$refgmass->{from} = $req->param('from');
		$refgmass->{to} = $req->param('to');
		$rawhtml = qq~	<br>
					<table border=0 >
					<tr style=\"background:#ADC4F1\">
						<td class="cell"><b>Время</b></td>
						<td class="cell"><b>Устройство</b></td>
						<td class="cell"><b>Пользователь</b></td>
						<td class="cell"><b>Источник</b></td>
						<td class="cell"><b>Результат</b></td>
					</tr>
						~;
		my $colorflag = 0;
		my $colorrow;
		$tmp = "";
		if ($req->param('device')) { $tmp .= qq~ AND device_ip = "${\$req->param('device')}"~;}
		if ($req->param('user')) { $tmp .= qq~ AND user = "${\$req->param('user')}"~;}
		if ($req->param('initiator')) { $tmp .= qq~ AND initiator = "${\$req->param('initiator')}"~;}
		if ($req->param('result')) { $tmp .= qq~ AND result = "${\$req->param('result')}"~;}
		my $sth = $dbh->prepare(qq(SELECT time,device_ip,user,initiator,result FROM access WHERE (time BETWEEN "$refgmass->{from}" and "$refgmass->{to}") $tmp ORDER by time));
		$sth->execute() or die $rawhtml .= $dbh->errstr();
		while (my $ref = $sth->fetchrow_hashref()) {
			if ($colorflag eq 0) {
				$colorrow = "#E8F0FF";
				$colorflag = 1;
			} else {
				$colorrow = "#CDDEFF"; 
				$colorflag = 0;
			} ;
			$rawhtml .= qq~
							<tr style=\"background:$colorrow\">
								<td class="cell">
									$ref->{time}
								</td>
								<td class="cell">
									$ref->{device_ip}
								</td>
								<td class="cell">
									$ref->{user}
								</td>
								<td class="cell">
									$ref->{initiator}
								</td>
								<td class="cell">
									$ref->{result}
								</td>
							</tr>
						~;
		}
		$rawhtml .= qq~
						</table>
						~;
	}
	return $rawhtml;
}

sub get_html {
	my $flag = shift;
	if ($flag eq "top"){
		return q~
				<!DOCTYPE html>
					<html lang="ru">
					<head>
						<meta http-equiv=Content-Type content="text/html;charset=UTF-8">
						<meta property="og:locale" content="ru_RU">
						<meta property="og:site_name" content="TACACSSQL">
						<title>TACACSPL</title>
						<style>
						   .cell {
							padding-left: 5px;
							padding-right: 5px;
						   }
						</style>
					</head>
					<body>~;
	}
	if ($flag eq "bottom"){
		return q~
					</body>
				</html>
		~;
	}
}

#маршрутизатор
my $main_app = builder {
    mount "/" => builder { $app; };
};
